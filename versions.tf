terraform {
  required_version = ">= 0.14.6"

  required_providers {
    aws = ">= 3.28"
  }
}