<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.14.6 |
| aws | >= 3.28 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.28 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| subnets | hashicorp/subnets/cidr |  |

## Resources

| Name |
|------|
| [aws_eip](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/eip) |
| [aws_internet_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/internet_gateway) |
| [aws_nat_gateway](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/nat_gateway) |
| [aws_region](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/data-sources/region) |
| [aws_route](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/route) |
| [aws_route_table](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/route_table) |
| [aws_route_table_association](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/route_table_association) |
| [aws_subnet](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/subnet) |
| [aws_vpc](https://registry.terraform.io/providers/hashicorp/aws/3.28/docs/resources/vpc) |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| azs | The Availbility zones in which should be create subnets. | `list(string)` | n/a | yes |
| cidr\_block | The cidr block of the desired VPC. | `string` | n/a | yes |
| create\_nat\_gateway | Determinates if create nat gateways for private routes or not | `bool` | `true` | no |
| enable\_dns\_hostnames | Whether or not the VPC has DNS hostname support | `bool` | `true` | no |
| enable\_dns\_support | Whether or not the VPC has DNS support. | `bool` | `true` | no |
| instance\_tenancy | The allowed tenancy of instances launched into the selected VPC. May be any of "default", "dedicated", or "host". | `string` | `"default"` | no |
| name | Name to be used on all the resources as identifier | `string` | n/a | yes |
| networks | The networks. | <pre>list(object({<br>    id          = string<br>    new_bits    = number<br>    route_table = string<br>  }))</pre> | n/a | yes |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| igw\_arn | The ARN of the Internet Gateway. |
| igw\_id | The ID of the Internet Gateway. |
| nat\_gw\_ids | The NAT Gateway ids. |
| nat\_gw\_private\_ips | The NAT Gateway private ips. |
| nat\_gw\_public\_ips | The NAT Gateway public ips. |
| rt\_id\_by\_name | The route table ids by route table name. |
| subnect\_cidr\_blocks | The subnets cidr blocks |
| subnet\_ids | The subnet ids. |
| subnet\_ids\_by\_id | The subnets ids for specific network. |
| vpc\_arn | Amazon Resource Name (ARN) of VPC. |
| vpc\_cidr\_block | The CIDR block of the VPC. |
| vpc\_enable\_dns\_hostnames | Whether or not the VPC has DNS hostname support. |
| vpc\_enable\_dns\_support | Whether or not the VPC has DNS support. |
| vpc\_id | The ID of the VPC. |
| vpc\_instance\_tenancy | Tenancy of instances spin up within VPC. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->