##########################################################################
### VPC
##########################################################################

resource "aws_vpc" "this" {
  cidr_block           = var.cidr_block
  instance_tenancy     = var.instance_tenancy
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    var.tags
  )
}

##########################################################################
### SUBNET
##########################################################################

module "subnets" {
  source = "hashicorp/subnets/cidr"

  base_cidr_block = var.cidr_block
  networks        = local.networks
}

resource "aws_subnet" "this" {
  for_each = module.subnets.network_cidr_blocks

  vpc_id            = aws_vpc.this.id
  cidr_block        = each.value
  availability_zone = format("%s%s", local.region, split("_", each.key)[1])

  tags = merge(
    {
      "Name" = each.key
    },
    var.tags
  )
}

##########################################################################
### Internet Gateway
##########################################################################

resource "aws_internet_gateway" "this" {
  count = local.public_subnets_count > 0 ? 1 : 0

  vpc_id = aws_vpc.this.id
  tags = merge(
    {
      "Name" = format("%s", var.name)
    },
    var.tags
  )
}

##########################################################################
### ELASTIC IP
##########################################################################

resource "aws_eip" "nat" {
  for_each = local.nat_eips

  vpc = true
  tags = merge(
    {
      "Name" = each.key
    },
    var.tags
  )
}

##########################################################################
### NAT GATEWAY
##########################################################################

resource "aws_nat_gateway" "this" {
  for_each = local.nat_gateways

  allocation_id = aws_eip.nat[each.key].id
  subnet_id     = aws_subnet.this[each.key].id
  tags = merge(
    {
      "Name" = each.key
    },
    var.tags
  )
}

##########################################################################
### ROUTE
##########################################################################

resource "aws_route" "public_internet_gateway" {
  for_each = { for net in local.route_tables : net => net if net == "public" }

  route_table_id         = aws_route_table.this[each.key].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this[0].id

  timeouts {
    create = "5m"
  }
}

resource "aws_route" "private_nat_gateway" {
  for_each = { for x in local.nat_gateways_routes : x.nat_gateway_id => x.route_table_id }

  route_table_id         = aws_route_table.this[each.value].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.this[each.key].id

  timeouts {
    create = "5m"
  }
}

##########################################################################
### ROUTE TABLE
##########################################################################

resource "aws_route_table" "this" {
  for_each = { for rt in local.route_tables : rt => rt }

  vpc_id = aws_vpc.this.id
  tags = merge(
    {
      "Name" = each.key
    },
    var.tags
  )
}

##########################################################################
### ROUTE TABLE ASSOCIATION
##########################################################################

resource "aws_route_table_association" "this" {
  for_each = { for net in local.networks : net.name => net.route_table }

  subnet_id      = aws_subnet.this[each.key].id
  route_table_id = aws_route_table.this[each.value].id
}
